package com.string.manipulation;

public class StringReverse {
	public static void main(String[] args) {
		StringBuffer s = new StringBuffer("shabaz");
		System.out.println(s.reverse());

		String s1 = "shabaz";
		char[] ch = s1.toCharArray();
		char[] chr = new char[s1.length()];
		int j = ch.length - 1;

		for (int i = 0; i < ch.length; i++) {

			chr[j] = ch[i];
			j--;
		}

		System.out.println(chr);

	}
}
